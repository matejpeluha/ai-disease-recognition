function inputs = getInputs(dataFileName)
M = csvread(dataFileName);
inputs = M(:,1:19)';
end