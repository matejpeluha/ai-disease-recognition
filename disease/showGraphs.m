function showGraphs(targets, outputs, tr)
figure, plotperform(tr);
figure, plotconfusion(targets, outputs);
end