function percentageGraphByNymberOfNeuronsTwoLayers(inputs, targets)
bestResult = [0, 0, 0];
index = 1;
for netSize1 = 30 : 50
for netSize2 = 18 : 25
netSize = [netSize1 netSize2];

net = createPreSettedNet(netSize);
[net, ~] = train(net, inputs, targets);

outputs = net(inputs);
result = getSuccesPercentage(targets, outputs);

fprintf("c" + netSize1 + "/" + netSize2 + " = " + result + "\n");

if(result > bestResult(1))
    bestResult = [result, netSize1, netSize2];
end

numberOfNeurons1(index) = netSize1;
numberOfNeurons2(index) = netSize2;
allResults(index) = result;
index = index + 1;
end
end

fprintf("Najlepsi vysledok je: " + bestResult(1) + " s poctom neuronov: " + bestResult(2) + "/" + bestResult(3));

set(0, 'DefaultFigureRenderer', 'painters');
figure(1)
plot3(numberOfNeurons1, numberOfNeurons2, allResults, 'or');
hold on;
legend('uspesnost', 'Location','southeast')
xlabel('pocet neuronov v prvej vrstve');
ylabel('pocet neuronov v druhej vrstve');
zlabel('uspesnost');
hold off;
end