function stats = getStatsFromNet(netSize, inputs, targets)
min = 100;
max = 0;
average = 0;
averageTime = 0;

loops = 10;
for i = 1 : loops
startTime = tic;
    
net = createPreSettedNet(netSize);     
[net,~] = train(net, inputs, targets);

outputs = net(inputs);
c = getSuccesPercentage(targets, outputs);

fprintf(i + "c" + netSize + " = " + c + "\n");

if(c < min)
    min = c;
end
if(c > max)
    max = c;
end

average = average + c;

endTime = toc(startTime);

averageTime = averageTime + endTime;
end

average = average / loops;

averageTime = averageTime / loops;

fprintf("averageTime = " + averageTime + "\n");

stats = [min, max, average, netSize];

end