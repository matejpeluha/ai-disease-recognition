function percentageGraphByNumberOfNeuronsOneLayer(inputs, targets)
bestMinSet = [0, 0, 0, 1];
bestMaxSet = [0, 0, 0, 1];
bestAverageSet = [0, 0, 0, 1];

for netSize = 1 : 150

stats = getStatsFromNet(netSize , inputs, targets);

if(stats(1) > bestMinSet(1))
    bestMinSet = stats;
end
if(stats(2) > bestMaxSet(2))
    bestMaxSet = stats;
end
if(stats(3) > bestAverageSet(3))
    bestAverageSet = stats;
end

allAverages(netSize) = stats(3);
allMaximums(netSize) = stats(2);
allMinimums(netSize) = stats(1);
numberOfNeurons(netSize) = netSize;
end
fprintf("min, average, max, set \n");
fprintf("bestMinSet = " + bestMinSet(1) + " : " + bestMinSet(3) + " : " + bestMinSet(2) + " = " +  "\n");
fprintf("bestMaxSet = " + bestMaxSet(1) + " : " + bestMaxSet(3) + " : " + bestMaxSet(2) + " = " +  "\n");
fprintf("bestAverageSet = " + bestAverageSet(1) + " : " + bestAverageSet(3) + " : " + bestAverageSet(2) + " = " +  "\n");

set(0, 'DefaultFigureRenderer', 'painters');
figure(1)
plot(numberOfNeurons, allMaximums, 'r');
hold on;
%plot(numberOfNeurons, allAverages, 'g');
%plot(numberOfNeurons, allMinimums, 'b');
legend('najvacsia uspesnost', 'priemerna uspesnost', 'najmensia uspesnost', 'Location','southeast')
xlabel('pocet epoch pri trenovani siete');
ylabel('uspesnost');
hold off;
end