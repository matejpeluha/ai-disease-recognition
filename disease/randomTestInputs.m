function [newInputs, newTargets] = randomTestInputs(inputs, targets, inputsSize)
inputs = inputs';
targets = targets';
[realSizeInputs, ~] = size(inputs);

if(inputsSize > realSizeInputs)
   inputsSize = realSizeInputs;
end

rng(0,'twister');
rng('shuffle');
r = randi([1 realSizeInputs], 1, inputsSize);

for i = 1 : inputsSize 
    index = r(1, i);
    newInputs(i, : ) = inputs(index, : );
    newTargets(i, : ) = targets(index, : );
end

newInputs = newInputs';
newTargets = newTargets';


end