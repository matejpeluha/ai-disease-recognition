function net = createPreSettedNet(netSize)
net = patternnet(netSize); 

net.performFcn = 'crossentropy';
net.trainParam.goal = 0.0001;	    % Ukoncovacia podmienka na chybu SSE.
net.trainParam.epochs = 1200;  	    % Max. pocet trénovacích cyklov.
net.trainParam.min_grad = 1e-10;
%net.trainParam.max_fail = 1000;


net.divideFcn='dividerand';
net.divideParam.trainRatio=0.6; % 60% trenovacie
net.divideParam.valRatio=0.0;
net.divideParam.testRatio=0.4;
end