function c = getSuccesPercentage(targets, outputs)
[c,~] = confusion(targets,outputs);
c = 100 * (1-c);
end