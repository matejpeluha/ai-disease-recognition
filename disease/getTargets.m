function outputs = getTargets(fileName)

% Načítame dataset zo súboru
M = csvread(fileName);

% Oddelíme vektor výstupov
t = M(:,20);
output = zeros(size(t, 1), 2);      % vystupne data pre NS
% Vytvoríme maticu žiadaných hodnôt
for i = 1 : size(t,1)
    output(i, t(i) + 1) = 1;
end
outputs = output';

end