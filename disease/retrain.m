function [result, endTime] = retrain(inputs, targets)
startTime = tic;

net = createPreSettedNet(40);  


[~, pop] = size(inputs);

trainInputs = inputs( : , 1 : pop - 100);
useInputs = inputs( : , pop - 100 : end);

trainTargets = targets( : , 1 : pop - 100);
useTargets = targets( : , pop - 100 : end);

average = 0;

numOfTrainings = 15;
for i = 1 : numOfTrainings
[net, tr] = train(net, trainInputs, trainTargets);

outputs = net(trainInputs);
result = getSuccesPercentage(trainTargets, outputs);

fprintf( i + "c : " + result + "\n"); 

average = average + result;
end

average = average / numOfTrainings;

endTime = toc(startTime);

fprintf("Average: " + average + "\n");
fprintf("Time: " + endTime);

outputs = net(useInputs);
showGraphs(useTargets, outputs, tr);

end