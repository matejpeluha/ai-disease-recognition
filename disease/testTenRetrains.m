function testTenRetrains(inputs, targets)

maxResult = 0;
minResult = 100;
averageResult = 0;

averageTime = 0;

loops = 10;
for i = 1 : loops
[result, time] = retrain(inputs, targets);

if (result > maxResult)
    maxResult = result;
end
if (result < minResult)
    minResult = result;
end

averageResult = averageResult + result;
averageTime = averageTime + time;

end
averageResult = averageResult / loops;
averageTime = averageTime / loops;

fprintf("Max: " + maxResult + "\n");
fprintf("Average: " + averageResult + "\n");
fprintf("Min: " + minResult + "\n");
fprintf("\n AverageTime: " + averageTime + "\n");
end